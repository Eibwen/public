<Query Kind="Program">
  <Connection>
    <ID>eb3222b4-dc96-4405-8f78-11ab6a20332a</ID>
    <Persist>true</Persist>
    <Driver Assembly="IQDriver" PublicKeyToken="5b59726538a49684">IQDriver.IQDriver</Driver>
    <Provider>System.Data.SQLite</Provider>
    <AttachFileName>D:\Projects\CSharp\!Personal\WallpaperDownload\TESTING\New\UsedColorsCount.sqlite</AttachFileName>
    <NoPluralization>true</NoPluralization>
    <NoCapitalization>true</NoCapitalization>
    <CustomCxString>Data Source=D:\Projects\CSharp\!Personal\WallpaperDownload\TESTING\New\UsedColorsCount.sqlite</CustomCxString>
    <DriverData>
      <CreateDbIfMissing>true</CreateDbIfMissing>
    </DriverData>
  </Connection>
</Query>

void Main()
{
	//string encoded = @"gGOCgwBZWQCDiYqLjI2Oj5CRigCFhpaXmJmZlIycY5SHoJaIkpCUlYaepausra6vsLGys7S1tre4ubq7vL2+v8DBwsPExcbHyMnKy8zNzs/Q0dLT1NXW19jZ2tvc3d7f4OHi4+Tl5ufo6err7OuFpOyomvOb8JP2n/Kpsaf9/e0AAwocSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx4+8TgH0R7IfPXyEUAoStc+VKJUgY8qcSbOmzZs4c+rcybOnz59AgwodSrSo0aNIkyYrCcqTP1MtW1VSpbSq1atYs2rdyrWr169gw4odS7as2bNo0wbzNyrSVJcA";
	string encoded = @"gGOCgwBZWQCDiYqLjI2Oj5CRigCFhpaXmJmZlIycY5SHoJaIkpCUlYaepausra6vsLGys7S1tre4ubq7vL2+v8DBwsPExcbHyMnKy8zNzs/Q0dLT1NXW19jZ2tvc3d7f4OHi4+Tl5ufo6err7OuFpOyomvOb8JP2n/Kpsaf9/e0AAwocSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx4+8TgH0R7IfPXyEUAoStc+VKJUgY8qcSbOmzZs4c+rcybOnz59AgwodSrSo0aNIkyYrCcqTP1MtW1VSpbSq1atYs2rdyrWr169gw4odS7as2bNo0wbzNyrSVJcA";
	int LZWMinimumCodeSize = 7;
	byte[] buffer = Convert.FromBase64String(encoded);
	
	int First = buffer[0];
	First <<= 8; First |= buffer[1];
	First <<= 8; First |= buffer[2];
	First <<= 8; First |= buffer[3];
	GetIntBinaryString(First).Dump();
	
	
	LZW l = new LZW(LZWMinimumCodeSize);
	using (MemoryStream ms = new MemoryStream(buffer))
	{
		l.Decode(ms, 254, null);
	}
}

//DEBUG HELPERS
static string GetIntBinaryString(int n)
{
	return GetIntBinaryString(n, 32);
}
static string GetIntBinaryString(int n, int size)
{
	char[] b = new char[size];
	int pos = size-1;
	int i = 0;
	
	while (i < size)
	{
		if ((n & (1 << i)) != 0)
		{
		b[pos] = '1';
		}
		else
		{
		b[pos] = '0';
		}
		pos--;
		i++;
	}
	return new string(b);
}


// Define other methods and classes here
public class LZW
{
	int minCodeSizeBits = 8;
	int curCodeSizeBits = 8;
	int CODE_MASK = 0xFF;
	int CLEAR_CODE = 0;
	
	public LZW(int LZWMinimumCodeSize)
	{
		SetCodeSize(LZWMinimumCodeSize);
	}
	
	void SetCodeSize(int MinCodeSizeBits)
	{
		curCodeSizeBits = minCodeSizeBits = MinCodeSizeBits+1;
		CODE_MASK = (1 << curCodeSizeBits) - 1;
		//if (CLEAR_CODE == -1) //I think clear code might be constant
		//CLEAR_CODE = 1 << (MinCodeSizeBits - 1);
		//CLEAR_CODE.Dump();
	}
	public void IncreaseCodeSize()
	{
		curCodeSizeBits++;
		CODE_MASK = (1 << curCodeSizeBits) - 1;
	}
	public void ClearCode()
	{
		curCodeSizeBits = minCodeSizeBits;
		CODE_MASK = (1 << curCodeSizeBits) - 1;
	}
	
	public void Encode(Stream input, int byteCount, Stream output)
	{
	}
	
	public void Decode(Stream input, int byteCount, Stream output)
	{
		int bitBuffer = 0;
		Decode(input, byteCount, output, ref bitBuffer);
	}
	public void Decode(Stream input, int byteCount, Stream output, ref int bitBuffer)
	{
		BidirectionalDictionary dict = new BidirectionalDictionary();
		
		//Bit reading...
		int avalibleBits = 0;
		//int bitBuffer = 0;
		for (int i = 0; i < byteCount; ++i)
		{
			//Shift existing buffer over
			bitBuffer <<= 8;
			//Read next byte
			bitBuffer |= input.ReadByte();
			//Set new count
			avalibleBits += 8;
			
			while (avalibleBits >= curCodeSizeBits)
			{
				//Reduce avalible bits because i'm using them
				avalibleBits -= curCodeSizeBits;
				//Copy to new int, shifting so the first codeSizeBits are what we want
				int code = (bitBuffer >> avalibleBits) & CODE_MASK;
				
				
				
				string k = dict.GetKey(code);
				if (k != null) (GetIntBinaryString(code, curCodeSizeBits) + " => " + (int)k[0]).Dump();
				else GetIntBinaryString(code, curCodeSizeBits).Dump();
				
				
				
				if (code == CODE_MASK)
				{
					"".Dump("INCREASE CODE SIZE");
					//SetCodeSize(minCodeSizeBits+1);
					IncreaseCodeSize();
				}
				if (code == CLEAR_CODE)
				{
					"".Dump("RESET DATABASE");
					ClearCode();
					dict.Reset();
				}
			}
		}
		avalibleBits.Dump("LEFT OVER BITS");
		GetIntBinaryString(bitBuffer).Dump();
	}
	
	
	
}

public class BidirectionalDictionary
{
	Dictionary<string, int> encTable = null;
	Dictionary<int, string> decTable = null;
	
	public Dictionary<string, int> EncodeTable { get { return encTable; } }
	public Dictionary<int, string> DecodeTable { get { return decTable; } }
	
	public void Add(string key, int value)
	{
		encTable.Add(key, value);
		decTable.Add(value, key);
	}
	
	public int GetValue(string key)
	{
		if (encTable.ContainsKey(key))
			return encTable[key];
		return -1;
	}
	public string GetKey(int value)
	{
		if (decTable.ContainsKey(value))
			return decTable[value];
		return null;
	}

	public BidirectionalDictionary()
	{
		Reset();
	}
	public void Reset()
	{
		encTable = new Dictionary<string, int>();
		decTable = new Dictionary<int, string>();
		
//		//Load ASCII values
//		for (int i = 0; i < 256; i++)
//		{
//			this.Add(((char)i).ToString(), i);
//		}
	}
}
