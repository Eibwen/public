<Query Kind="Program">
  <Connection>
    <ID>cbc81648-1a58-46af-bd4d-86140b8ee4dd</ID>
    <Persist>true</Persist>
    <Server>STJDQUOTE\SQLEXPRESS</Server>
    <Database>SedonaWorkflowMonitor</Database>
    <NoPluralization>true</NoPluralization>
    <NoCapitalization>true</NoCapitalization>
  </Connection>
  <Namespace>System.Drawing</Namespace>
  <Namespace>System.Runtime.InteropServices</Namespace>
</Query>

void Main()
{
	string TEMP_PATH = @"C:\Users\gwalker\AppData\Local\Temp\WeatherSS2\";
	
	string file = TEMP_PATH + "NAT_20120705_1858.gif";
	//103 colors
	
	GetPixelsRaw(file);
//	GetPixelsNaive(file);
//	GetPixelsFaster(file);
}

// Define other methods and classes here
public IEnumerable<Int32> GetPixelsRaw(string gifPath)
{
	//Supporting format Format8bppIndexed
	using (Stream fs = File.OpenRead(gifPath))
	{
		GregsGif gif = new GregsGif(fs);
		
		gif.Dump();
	}
	return null;
}

/// <summary>
/// Implimenting my own limited Gif reading classed based on the spec:
///	  http://www.w3.org/Graphics/GIF/spec-gif89a.txt
/// </summary>
public class GregsGif
{
	public GregsGif(Stream s)
	{
		Valid = true;
		
		//Header
		for (int i = 0; i < HeaderSignature.Length; ++i)
		{
			if (s.ReadByte() != HeaderSignature[i])
			{
				Valid = false;
				throw new ApplicationException("Header doesn't match 'GIF89a");
			}
		}
		for (int i = 0; i < HeaderVersion.Length; ++i)
		{
			if (s.ReadByte() != HeaderVersion[i])
			{
				Valid = false;
				throw new ApplicationException("Header doesn't match 'GIF89a");
			}
		}
		
		//Logical Screen Descriptor
//		byte[] sizeTemp = new byte[2];
//		s.Read(sizeTemp, 0, 2);
//		Width = IntFromLittleEndian(sizeTemp);
//		s.Read(sizeTemp, 0, 2);
//		Height = IntFromLittleEndian(sizeTemp);
		Width = s.ReadByte() | s.ReadByte() << 8;
		Height = s.ReadByte() | s.ReadByte() << 8;
		
		int packedFields = s.ReadByte();
		//packedFields.Dump();
		GlobalColorTable = (packedFields >> 7) == 1;
		ColorResolution = ((packedFields >> 4 & 0x7) + 1);
		Sort = (packedFields >> 3 & 1) == 1;
		//PaletteSize = (int)Math.Pow(2, (packedFields & 0x7) + 1);
		PaletteSize = (2 << (packedFields & 0x7));
		if (PaletteSize == 2) PaletteSize = 0;
		
		//GetIntBinaryString(packedFields).Dump();
		
		BackgroundColorIndex = s.ReadByte();
		int par = s.ReadByte();
		PixelAspectRatio = par == 0 ? 0 : ((par + 15) / 64);
		
		
		//Load the Palette
		//int paletteByteSize = 3*PaletteSize;
		Palette = LoadPalette(PaletteSize, s);
		
		
		Frames = new List<ImageDescriptor>();
		
		int Introducer = 0;
		do
		{
			Introducer = s.ReadByte();
			Introducer.ToString("X2").Dump();
			
			switch (Introducer)
			{
				case 0x2C:
					//Now we are on an Image Descriptor
					Frames.Add(new ImageDescriptor(Introducer, s));
					break;
				case 0x21:
					//Extension Introducer
					//This marks it as an extension, skip to next byte
					break;
				case 0xF9:
					//Graphic Control Extension
					new GraphicControlExtension(Introducer, s);
					break;
				case 0x3B:
					//End of Gif
					Complete = true;
					//s.Position.Dump("End position");
					Introducer = -1;
					break;
				
				case 0xFF:
					throw new NotSupportedException("Application Extension is not supported");
				case 0xFE:
					throw new NotSupportedException("Comment Extension is not supported");
				case 0x01:
					throw new NotSupportedException("Plain Text Extension is not supported");
				default:
					//Debugging:
					(s.ReadByte() << 24 | s.ReadByte() << 16 | s.ReadByte() << 8 | s.ReadByte()).ToString("X8").Dump("Next int");
					throw new NotImplementedException("Unknown Introducer: 0x" + Introducer.ToString("X2"));
			}
		}
		while (Introducer >= 0);
	}
	
	static int[] LoadPalette(int PaletteSize, Stream s)
	{
		int[] Palette = new int[PaletteSize];
		for (int i = 0; i < PaletteSize; ++i)
		{
			//Loop through the palette
			Palette[i] |= 0xFF;
			Palette[i] <<= 8;
			Palette[i] |= s.ReadByte();
			Palette[i] <<= 8;
			Palette[i] |= s.ReadByte();
			Palette[i] <<= 8;
			Palette[i] |= s.ReadByte();
			
//			Palette[i].ToString("X8").Dump();
//			GetIntBinaryString(Palette[i]).Dump();
		}
		return Palette;
	}
	
	static readonly byte[] HeaderSignature = new byte[] { 0x47, 0x49, 0x46 };
	static readonly byte[] HeaderVersion = new byte[] { 0x38, 0x39, 0x61 };
	public bool Valid { get; set; }
	public bool Complete { get; set; }
	
	public int Width { get; set; }
	public int Height { get; set; }
	
	/// <summary>
	/// Global Color Table Flag - Flag indicating the presence of a
	///		Global Color Table; if the flag is set, the Global Color Table will
	///		immediately follow the Logical Screen Descriptor. This flag also
	///		selects the interpretation of the Background Color Index; if the
	///		flag is set, the value of the Background Color Index field should
	///		be used as the table index of the background color. (This field is
	///		the most significant bit of the byte.)
	/// </summary>
	bool GlobalColorTable { get; set; }
	/// <summary>
	/// Color Resolution - Number of bits per primary color available
	///		to the original image, minus 1. This value represents the size of
	///		the entire palette from which the colors in the graphic were
	///		selected, not the number of colors actually used in the graphic.
	///		For example, if the value in this field is 3, then the palette of
	///		the original image had 4 bits per primary color available to create
	///		the image.  This value should be set to indicate the richness of
	///		the original palette, even if not every color from the whole
	///		palette is available on the source machine.
	/// </summary>
	int ColorResolution { get; set; }
	/// <summary>
	/// Sort Flag - Indicates whether the Global Color Table is sorted.
	///		If the flag is set, the Global Color Table is sorted, in order of
	///		decreasing importance. Typically, the order would be decreasing
	///		frequency, with most frequent color first. This assists a decoder,
	///		with fewer available colors, in choosing the best subset of colors;
	///		the decoder may use an initial segment of the table to render the
	///		graphic.
	/// </summary>
	bool Sort { get; set; }
	/// <summary>
	/// Size of Global Color Table - If the Global Color Table Flag is
	///		set to 1, the value in this field is used to calculate the number
	///		of bytes contained in the Global Color Table. To determine that
	///		actual size of the color table, raise 2 to [the value of the field
	///		+ 1].  Even if there is no Global Color Table specified, set this
	///		field according to the above formula so that decoders can choose
	///		the best graphics mode to display the stream in.  (This field is
	///		made up of the 3 least significant bits of the byte.)
	/// </summary>
	public int PaletteSize { get; set; }
	int BackgroundColorIndex { get; set; }
	double PixelAspectRatio { get; set; }
	
	public int[] Palette { get; set; }
	
	public List<ImageDescriptor> Frames { get; set; }
	
//	int IntFromLittleEndian(params byte[] b)
//	{
//		LittleEndian le = new LittleEndian();
//		//This is BigEndian?:
////		if (b.Length > 3) le.b1 = b[3];
////		if (b.Length > 2) le.b2 = b[2];
////		if (b.Length > 1) le.b3 = b[1];
////		if (b.Length > 0) le.b4 = b[0];
//		if (b.Length > 3) le.b4 = b[3];
//		if (b.Length > 2) le.b3 = b[2];
//		if (b.Length > 1) le.b2 = b[1];
//		if (b.Length > 0) le.b1 = b[0];
//		
//		return le.i;
//	}
//	[StructLayoutAttribute(LayoutKind.Explicit)]
//	struct LittleEndian
//	{
//		[FieldOffset(0)]
//		public byte b1;
//		[FieldOffset(1)]
//		public byte b2;
//		[FieldOffset(2)]
//		public byte b3;
//		[FieldOffset(3)]
//		public byte b4;
//		[FieldOffset(0)]
//		public int i;
//	}
	
	
	public class ImageDescriptor
	{
		public ImageDescriptor(int imageSeparator, Stream s)
		{
			ImageSeparator = imageSeparator;
			ImageLeftPosition = s.ReadByte() | s.ReadByte() << 8;
			ImageTopPosition = s.ReadByte() | s.ReadByte() << 8;
			ImageWidth = s.ReadByte() | s.ReadByte() << 8;
			ImageHeight = s.ReadByte() | s.ReadByte() << 8;
			
			int packedFields = s.ReadByte();
			//packedFields.Dump();
			LocalColorTable = (packedFields >> 7) == 1;
			Interlaced = (packedFields >> 6 & 1) == 1;
			Sort = (packedFields >> 5 & 1) == 1;
			//PaletteSize = (int)Math.Pow(2, (packedFields & 0x7) + 1);
			PaletteSize = (2 << (packedFields & 0x7));
			if (PaletteSize == 2) PaletteSize = 0;
			//PaletteSize.Dump("LocalPalette");
			
			Palette = LoadPalette(PaletteSize, s);
			
			LoadImageData(s);
		}
		
		int ImageSeparator { get; set; }
		int ImageLeftPosition { get; set; }
		int ImageTopPosition { get; set; }
		int ImageWidth { get; set; }
		int ImageHeight { get; set; }
		/// <summary>
		/// Local Color Table Flag - Indicates the presence of a Local Color
		///    Table immediately following this Image Descriptor. (This field is
		///    the most significant bit of the byte.)
		/// </summary>
		bool LocalColorTable { get; set; }
		/// <summary>
		/// Interlace Flag - Indicates if the image is interlaced. An image
		///    is interlaced in a four-pass interlace pattern; see Appendix E for
		///    details.
		/// </summary>
		bool Interlaced { get; set; }
		/// <summary>
		/// Sort Flag - Indicates whether the Local Color Table is
		///    sorted.  If the flag is set, the Local Color Table is sorted, in
		///    order of decreasing importance. Typically, the order would be
		///    decreasing frequency, with most frequent color first. This assists
		///    a decoder, with fewer available colors, in choosing the best subset
		///    of colors; the decoder may use an initial segment of the table to
		///    render the graphic.
		/// </summary>
		bool Sort { get; set; }
	//	bool reserved1 { get; set; }
	//	bool reserved2 { get; set; }
		/// <summary>
		/// Size of Local Color Table - If the Local Color Table Flag is
		///    set to 1, the value in this field is used to calculate the number
		///    of bytes contained in the Local Color Table. To determine that
		///    actual size of the color table, raise 2 to the value of the field
		///    + 1. This value should be 0 if there is no Local Color Table
		///    specified. (This field is made up of the 3 least significant bits
		///    of the byte.)
		/// </summary>
		public int PaletteSize { get; set; }
		public int[] Palette { get; set; }
		
		void LoadImageData(Stream s)
		{
			int LZWMinimumCodeSize = s.ReadByte();
			LZWMinimumCodeSize.Dump("Min Code Size");
			
			//int BitMask = (1 << LZWMinimumCodeSize) -1;
			byte[] buffer = new byte[255];
			int imageDataSize = s.ReadByte();
			while (imageDataSize > 0)
			{
				int readData = s.Read(buffer, 0, imageDataSize);
				("Wanted: " + imageDataSize + "  Got: " + readData).Dump();
				Convert.ToBase64String(buffer).Dump();
				LZWDecoder dec = new LZWDecoder(LZWMinimumCodeSize);
				string f = dec.DecodeFromCodes(buffer);
				("decoded: " + f.Length).Dump();
				//throw new ApplicationException();
				
				imageDataSize = s.ReadByte();
			}
		}
	}
	
	/// <summary>
	/// The Graphic Control Extension contains parameters used
	///  when processing a graphic rendering block. The scope of this extension is
	///  the first graphic rendering block to follow. The extension contains only
	///  one data sub-block.
	///
	///  This block is OPTIONAL; at most one Graphic Control Extension may precede
	///  a graphic rendering block. This is the only limit to the number of
	///  Graphic Control Extensions that may be contained in a Data Stream.
	/// </summary>
	public class GraphicControlExtension
	{
		public GraphicControlExtension(int graphicControlLabel, Stream s)
		{
			//ExtensionIntroducer = extensionIntroducer;
			GraphicControlLabel = graphicControlLabel;
			BlockSize = s.ReadByte();
			//BlockSize.Dump("Block Size");
			
			int packedFields = s.ReadByte();
			DisposalMethod = (DisposalMethodVal)(packedFields >> 2 & 0x7);
			UserInputFlag = (packedFields >> 1 & 1) == 1;
			TransparentColorFlag = (packedFields & 1) == 1;
			//GetIntBinaryString(packedFields).Dump();
			
			DelayTime = s.ReadByte() << 8 | s.ReadByte();
			TransparentColorIndex = s.ReadByte();
			BlockTerminator = s.ReadByte();
		}
		
		int ExtensionIntroducer { get; set; } //For this always 0x21
		int GraphicControlLabel { get; set; }
		int BlockSize { get; set; }
//		Reserved1
//		Reserved2
//		Reserved3
		DisposalMethodVal DisposalMethod { get; set; }
		public enum DisposalMethodVal : byte
		{
			NoneDefined = 0,
			Donotdispose = 1,
			RestoreToBG = 2,
			RestoreToPrev = 3
			//ToBeDefined = 4,
			//ToBeDefined = 5,
			//ToBeDefined = 6,
			//ToBeDefined = 7
		}
		bool UserInputFlag { get; set; }
		bool TransparentColorFlag { get; set; }
		int DelayTime { get; set; }
		int TransparentColorIndex { get; set; }
		int BlockTerminator { get; set; }
	}
}
//http://bytes.com/topic/c-sharp/answers/238589-int-byte

static string GetIntBinaryString(int n)
{
	char[] b = new char[32];
	int pos = 31;
	int i = 0;
	
	while (i < 32)
	{
		if ((n & (1 << i)) != 0)
		{
		b[pos] = '1';
		}
		else
		{
		b[pos] = '0';
		}
		pos--;
		i++;
	}
	return new string(b);
}


#region LZW Decompression
/*

		This is a simple implementation of the well-known LZW algorithm. 
	Copyright (C) 2011  Stamen Petrov <stamen.petrov@gmail.com>

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

//http://code.google.com/p/sharp-lzw/source/browse/trunk/SharpLZW/LZW/LZWDecoder.cs
public class LZWDecoder
{
	public Dictionary<string, int> dict = new Dictionary<string, int>();
	int codeLen = 8;
	ANSI table;
	public LZWDecoder()
	{
		table = new ANSI();
		dict = table.Table;         
	}
	public LZWDecoder(int CodeLen)
	{
		codeLen = CodeLen;
		table = new ANSI();
		dict = table.Table;         
	}

	public string DecodeFromCodes(byte[] bytes)
	{
		string output = GetBinaryString(bytes);            

		return Decode(output);
	}

	public string Decode(string output)
	{
		StringBuilder sb = new StringBuilder();

		int i = 0;
		string w = "";
		int prevValue = -1; 

		while (i < output.Length)
		{
			if (i + codeLen + 8 <= output.Length)
			{
				w = output.Substring(i, codeLen);
			}
			else if (i + codeLen <= output.Length)
			{
				int encodedLen = i + codeLen;
				int trimBitsLen = output.Length - encodedLen;

				w = output.Substring(i, codeLen - (8 - trimBitsLen)) + output.Substring(output.Length - (8 - trimBitsLen), (8 - trimBitsLen));
			}
			else
			{
				break;
			}

			i += codeLen;

			int value = Convert.ToInt32(w, 2);

			string key = FindKey(dict, value);
			string prevKey = FindKey(dict, prevValue);

			if (prevKey == null)
			{
				prevKey = "";
			}

			if (key == null)
			{
				//handles the situation cScSc
				key = prevKey;

				sb.Append(prevKey + (key.Length > 0 ? key.Substring(0, 1) : ""));
			}
			else
			{
				sb.Append(key);
			}

			string finalKey = prevKey + (key.Length > 0 ? key.Substring(0, 1) : "");

			if (dict.ContainsKey(finalKey) == false)
			{
				dict[finalKey] = dict.Count;
			}

			if (Convert.ToString(dict.Count, 2).Length > codeLen)
				codeLen++;

			prevValue = value;
		}

		return sb.ToString();
	}
}

//http://code.google.com/p/sharp-lzw/source/browse/trunk/SharpLZW/LZW/ANSI.cs
public class ANSI
{
	Dictionary<string, int> table = new Dictionary<string, int>();
	public Dictionary<string, int> Table
	{
		get
		{
			return table;
		}
	}

	public ANSI()
	{
		for (int i = 0; i < 256; i++)
		{
			table.Add(System.Text.Encoding.Default.GetString(new byte[1] { Convert.ToByte(i) }), i);
		}
	}

//	public void WriteLine()
//	{
//		table.WriteLine();
//	}
//
//	public void WriteToFile()
//	{
//		File.WriteAllText("ANSI.txt", table.ToStringLines(), Encoding.Default);
//	}
}

//This seems really poor, are the values unique? I.e. have a reverse mapping dictionary as well
public static string FindKey(IDictionary<string, int> lookup, int value)
{
	foreach (var pair in lookup)
	{
		if (pair.Value == value)
		{
			return pair.Key;
		}
	}

	return null;
}
public static string GetBinaryString(byte[] bytes)
{
	StringBuilder sb = new StringBuilder();

	BitArray ba = new BitArray(bytes);
	for (int i = 0; i < ba.Length; ++i)
	{
		sb.Append(Convert.ToInt32(ba[i]).ToString());
	}

	return sb.ToString();
}

#endregion LZW Decompression





#region Naive
public long GetPixelsNaive(string filename)
{
	FileInfo fi = new FileInfo(filename);
	if (fi.Exists && fi.Length == 0)
	{
		//Image.FromFile will give an OutOfMemory exception when given an empty file
		filename.Dump("ERROR: Empty file");
		return 0;
	}
	
	using (Bitmap img = (Bitmap)Image.FromFile(filename))
	{
		var allPixels = GetPixels(img);
		Util.Progress = null;
		var allCounts = from p in allPixels
						group p by p into g
						select new { ColorID = g.Key, Count = g.Count() };
allCounts.ToList(); //Make sure i get the data
	}
	
	//Return length because that is slightly easier than pixels
	return fi.Length;
}
public IEnumerable<Int32> GetPixels(Bitmap img)
{
	for (int y = 0; y < img.Height; ++y)
	{
		Util.Progress = y * 100 / img.Height;
		for (int x = 0; x < img.Width; ++x)
		{
			yield return img.GetPixel(x, y).ToArgb();
		}
	}
}
#endregion Naive

#region Faster
public long GetPixelsFaster(string filename)
{
	FileInfo fi = new FileInfo(filename);
	if (fi.Exists && fi.Length == 0)
	{
		//Image.FromFile will give an OutOfMemory exception when given an empty file
		filename.Dump("ERROR: Empty file");
		return 0;
	}
	
	using (Bitmap img = (Bitmap)Image.FromFile(filename))
	{
		Rectangle rect = new Rectangle(0, 0, img.Width, img.Height);
		System.Drawing.Imaging.BitmapData bmpData =
			img.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadOnly, img.PixelFormat);
		
		var allPixels = GetPixels(bmpData, img.Palette.Entries);
		//var allPixels = GetPixelsUnsafe(bmpData, img.Palette.Entries);
		Util.Progress = null;
		var allCounts = from p in allPixels
						group p by p into g
						select new { ColorID = g.Key, Count = g.Count() };
allCounts.ToList(); //Make sure i get the data
		img.UnlockBits(bmpData);
	}
	
	//Return length because that is slightly easier than pixels
	return fi.Length;
}

public IEnumerable<Int32> GetPixels(System.Drawing.Imaging.BitmapData bmpData, Color[] palette)
{
	if (bmpData.PixelFormat != System.Drawing.Imaging.PixelFormat.Format8bppIndexed) throw new NotSupportedException();
	
	IntPtr ptr = bmpData.Scan0;
	//Copy each pixel into a byte
	int bytes  = Math.Abs(bmpData.Stride) * bmpData.Height;
	byte[] pixelValues = new byte[bytes];
	System.Runtime.InteropServices.Marshal.Copy(ptr, pixelValues, 0, bytes);
	
	int PixelSize = 1;
	
	for (int y = 0; y < bmpData.Height; ++y)
	{
		Util.Progress = y * 100 / bmpData.Height;
		for (int x = 0; x < bmpData.Width; ++x)
		{
			yield return palette[pixelValues[y*bmpData.Stride + x*PixelSize]].ToArgb();
		}
	}
	pixelValues = null;
}
#endregion Faster