// ==UserScript==
// @name        SedonaWebTimesheets Improve
// @namespace   EibwenScripts
// @description Make web timesheets worth something
// @downloadURL https://bitbucket.org/Eibwen/public/raw/master/SedonaWebTimesheetsImprove.user.js
// @require        http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js
// @include     https://webtimesheets.sedonatek.com/Main.aspx
// @match     https://webtimesheets.sedonatek.com/Main.aspx
// @version     1.0
// ==/UserScript==

/*
Adds:
Grays out any 0's in the grid, any entered numbers are black to make them stand out
Weekend columns have a gray background to stand out
Forward and backward buttons added, instead of using the shitty dropdown all the time
Session will not time out while the window is left open
*/

function addFunction(func) {
  var script = document.createElement("script");
  script.textContent = func;
  document.body.appendChild(script);
}
function addScriptRef(url) {
  var script = document.createElement("script");
  script.setAttribute("type","text/javascript")
  script.setAttribute("src", url)
  document.body.appendChild(script);
}
function addFunctionRun(func, exec) {
  var script = document.createElement("script");
  script.textContent = "-" + func + (exec ? "()" : "");
  document.body.appendChild(script);
}



var initalize = function(){
	addScriptRef('https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js');
	
	//===Add function and run it on the page, add jQuery reference to page
	addFunction(improveTimesheetStyling);
	//improveTimesheetStyling();
	location.assign('javascript:improveTimesheetStyling();void(0)');
	

	//===Set the events to include the styling changes
	location.assign('javascript:$find("ContentPlaceHolder1_RadAjaxManager1").OnResponseEnd = "ajaxTimesheetResponseEnd()";void(0)');
	location.assign('javascript:$find("ContentPlaceHolder1_RadAjaxManager1")._clientEvents.OnResponseEnd = "ajaxTimesheetResponseEnd()";void(0)');
	//location.assign('javascript:$find("ContentPlaceHolder1_RadAjaxManager1").set_clientEvents("OnResponseEnd", "ajaxTimesheetResponseEnd");void(0)');
	//location.assign('javascript:$find("ContentPlaceHolder1_RadAjaxManager1").get_clientEvents().OnResponseEnd = "ajaxTimesheetResponseEnd";void(0)');
	addFunction(ajaxTimesheetResponseEnd);
	
	
	//==Add next/last button events
	addFunction(prevWeek);
	addFunction(nextWeek);
	//==Add next/last buttons
	var dropdown = $("#ContentPlaceHolder1_ctl00\\$ContentPlaceHolder1\\$WeekEndingCBPanel").parent();
	dropdown.before('<div style="float: left;"><button onclick="return prevWeek()">&laquo;</button></div>');
	dropdown.after('<div style="float: left; padding-right:0.5em"><button onclick="return nextWeek()">&raquo;</button></div>');
	dropdown.css('width', '');
}
function improveTimesheetStyling(){
	//===Set zero values to lightgray
	$("td").each(function() {
		//This check sucks, but the project names are there
		var val = $(this).text();
		if ($.isNumeric(val) && val != 0)
		{
			//$(this).css('font-weight', 'bold');
		}
		else if ($.isNumeric(val))
		{
			$(this).css('color', 'lightgray');
		}
	});
	
	//===Set weekend columns to background gray
	$("#ContentPlaceHolder1_RadGrid1_ctl00").find("tr.rgRow, tr.rgAltRow").find("td:nth-child(9), td:nth-child(10)")
		.add("#ContentPlaceHolder1_PTORadGrid_ctl00 tr.rgRow td:nth-child(8),#ContentPlaceHolder1_PTORadGrid_ctl00 tr.rgRow td:nth-child(9)")
		.each(function() {
			$(this).css('background-color', 'lightgray')
					.css('color', 'gray');
			
			//Set back to default color if its not 0
			var val = $(this).text();
			if ($.isNumeric(val) && val != 0)
			{
				$(this).css('color', '');
			}
	});
}
initalize();


//Keep the session alive as long as the page is up
function keepAlive() {
 //console.log("Keeping Alive");
 $.post("Main.aspx", function(data) {
   //console.log(data);
 });
 window.setTimeout(keepAlive, 300000);
}
window.setTimeout(keepAlive, 300000);


function prevWeek() {
	var combo = $find("ContentPlaceHolder1_WeekEndingCB");
	var selected = combo.get_selectedIndex();
	combo.get_items().getItem(selected+1).select();
	return false;
}
function nextWeek() {
	var combo = $find("ContentPlaceHolder1_WeekEndingCB");
	var selected = combo.get_selectedIndex();
	combo.get_items().getItem(selected-1).select();
	return false;
}
function ajaxTimesheetResponseEnd(ajaxPanel, eventArgs) {
	improveTimesheetStyling();
}
